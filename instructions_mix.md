# BMS Alarms
<img src="https://bitbucket.org/pizzaoven/bmsalarmsassets/raw/3f0ca0adf3fa056107c792edcd2b8dfe8741ea71/images/fd_splash_black.png" width="150"/>
## _App Usage Instructions_
BMSAlarms is a tool for handling alarms of a BMS system. In order to use it, at least three inputs are required: a Company Token, a user name, and a Building Token.
The tokens are read through QR codes. The following are publicly available for test users:
![](https://bitbucket.org/pizzaoven/bmsalarmsassets/raw/c9a2a7fa9ab617b2019839ee2dd13ee998778766/images/poc_qr_codes.png)
In order to setup the app, follow these instructions:
- Press the indicated button to scan the building QR code
<img src="https://bitbucket.org/pizzaoven/bmsalarmsassets/raw/c9a2a7fa9ab617b2019839ee2dd13ee998778766/images/settings/00_settings_comp.png" width="200"/>
- You will be prompted to scan a QR code. Scan the "Company Token" QR code.
- Type your name on the next field.
- Lastly, you need to add at least one building token. Press the plus button, and you will be prompted to scan another QR code:
<img src="https://bitbucket.org/pizzaoven/bmsalarmsassets/raw/2ea157fcbc4598d10475cf6e6099b0eb071f2528/images/settings/01_settings_building.png" width="200"/>
- Scan the "Building Token" QR code. Now, the app should be ready to use.